//
//  DatabaseUtils.swift
//  Destiny Raid Scheduler
//
//  Created by Daniel Cherubini on 2/11/2015.
//  Copyright © 2015 Daniel Cherubini. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class DatabaseUtil: AnyObject {
    
    static func getCurrentManagedContext() -> NSManagedObjectContext {
        return (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    }
    
    static func saveDate(date: NSDate) {
        Events.addEvent(self.getCurrentManagedContext(), date: date)
    }
    
}