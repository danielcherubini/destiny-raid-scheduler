//
//  Events+CoreDataProperties.swift
//  Destiny Raid Scheduler
//
//  Created by Daniel Cherubini on 2/11/2015.
//  Copyright © 2015 Daniel Cherubini. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Events {

    @NSManaged var timeStamp: NSDate?

}
