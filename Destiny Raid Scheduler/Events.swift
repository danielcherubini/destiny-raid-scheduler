//
//  Events.swift
//  Destiny Raid Scheduler
//
//  Created by Daniel Cherubini on 2/11/2015.
//  Copyright © 2015 Daniel Cherubini. All rights reserved.
//

import Foundation
import CoreData


class Events: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

    class func addEvent(moc: NSManagedObjectContext, date: NSDate) -> Events {
        let newItem = NSEntityDescription.insertNewObjectForEntityForName("Events", inManagedObjectContext: moc) as! Events
        newItem.timeStamp = date
        return newItem
    }
    
    class func getAll(moc: NSManagedObjectContext) -> [Events]? {
        let fetchRequest = NSFetchRequest(entityName:"Events")
        do {
            let fetchResults = try moc.executeFetchRequest(fetchRequest) as? [Events]
            return fetchResults
        } catch let error as NSError {
            print(error)
            return nil
        }
    }


}
